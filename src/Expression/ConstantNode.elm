module Expression.ConstantNode (init, update, view, Model, Action) where

import Html exposing (..)
import Html.Attributes exposing (style)
import Maybe
--import Html.Events exposing (onClick)


-- MODEL

type alias Model = Float

init : Float -> Model
init num =
  num


-- UPDATE

type Action = DoNothing

update : Action -> Model -> Model
update action model = model


-- VIEW

view : Signal.Address Action -> Model -> Html
view address model =
    div [ countStyle ] [ text (toString model) ]


countStyle : Attribute
countStyle =
  style
    [ ("font-size", "20px")
    , ("font-family", "monospace")
    , ("display", "inline-block")
    , ("width", "50px")
    , ("text-align", "center")
    ]
