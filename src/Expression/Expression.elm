module Expression.Expression (init,addAll,update,view, setOperation,Remove) where

import List
import Expression.ListHelper
import Maybe
import Html exposing (..)
import Html.Attributes exposing (style)
import Expression.Node as Node exposing(init,update,view, Model, Action)
--import Html.Events exposing (onClick)


-- MODEL

type OperationType = Equals | Add | Multiply | None
type alias Model =
    { nodes : List ( ID, Node.Model )
    , nextID : ID
    , operation : OperationType
    }

type alias ID = Int

init: Model
init =
  { nodes = [],
    nextID = 0,
    operation = None
  }

addAll: Model -> List(String) -> Model
addAll model list =
  List.map (addOne model) list
    |> mergeModels

addOne: Model -> String -> Model
addOne model value =
  let newModel = Node.parse value
      newNodes =
        case newModel of
          Nothing   -> model.nodes
          Just node -> model.nodes ++ [ (model.nextID, node) ]
  in
    { model |
        nodes <- newNodes,
        nextID <- model.nextID + 1
    }

mergeModels: List(Model) -> Model
mergeModels list =
  List.foldr mergeModel init list

mergeModel: Model -> Model -> Model
mergeModel first second =
    { first |
      nodes <- List.append first.nodes second.nodes,
      nextID <- max first.nextID second.nextID
    }

setOperation: String -> Model -> Model
setOperation opString model =
  let operation =
    case opString of
      "=" -> Equals
      "+" -> Add
      "*" -> Multiply
      _   -> None
  in
    { model |
      operation <- operation
    }

verifyNoFailedInit : Model -> List(String) -> Model
verifyNoFailedInit model list2 =
  if List.length model.nodes ==  List.length list2 then
    model
  else
    init

-- UPDATE

type Action
    = InsertNew
    | Insert String
    --| InsertAll (List String)
    | Remove
    | Modify ID Node.Action

update : Action -> Model -> Model
update action model =
  case action of
    InsertNew ->
      let newNode = ( model.nextID, Node.init )
          newNodes = model.nodes ++ [ newNode ]
      in
          { model |
              nodes <- newNodes,
              nextID <- model.nextID + 1
          }

    Insert value ->
      let newModel = Node.parse value
          newNodes =
            case newModel of
              Nothing   -> model.nodes
              Just node -> model.nodes ++ [ (model.nextID, node) ]
      in
          { model |
              nodes <- newNodes,
              nextID <- model.nextID + 1
          }

    --InsertAll list ->
    --  (List.map Insert list)
    --    |> List.head
    --    |> Maybe.withDefault init

    Remove ->
      { model | nodes <- List.drop 1 model.nodes }

    Modify id nodeAction ->
      let updateNode (nodeID, nodeModel) =
            if nodeID == id
                then (nodeID, Node.update nodeAction nodeModel)
                else (nodeID, nodeModel)
      in
          { model | nodes <- List.map updateNode model.nodes }


-- VIEW

--view : Signal.Address Action -> Model -> Html
--view address model =
--    div [] [ text (toString model) ]

view : Signal.Address Action -> Model -> Html
view address model =
  let nodes = List.map (viewNode address) model.nodes
      opString =
        case model.operation of
          Equals   -> "="
          Add      -> "+"
          Multiply -> "*"
          None     -> "?"
      --remove = button [ onClick address Remove ] [ text "Remove" ]
      --insert = button [ onClick address Insert ] [ text "Add" ]
  in
      div [] (List.intersperse (div [] [text opString]) nodes)


viewNode : Signal.Address Action -> (ID, Node.Model) -> Html
viewNode address (id, model) =
  Node.view (Signal.forwardTo address (Modify id)) model

