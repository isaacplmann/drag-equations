module Expression.Node (init,parse,update,view, Model, Action) where

import List
import String
import Char
import Html exposing (..)
import Html.Attributes exposing (style)
import Expression.ExpressionNode as Expression exposing (init, update, view, Model, Action)
import Expression.SymbolNode as Symbol exposing (init, update, view, Model, Action)
import Expression.ConstantNode as Constant exposing (init, update, view, Model, Action)
--import Html.Events exposing (onClick)


-- MODEL

type NodeModel
    = ExpressionNodeType Expression.Model
    | SymbolNodeType Symbol.Model
    | ConstantNodeType Constant.Model
type alias Model = NodeModel

init : Model
init =
    ConstantNodeType 0

parse : String -> Maybe Model
parse value =
  if | String.all Char.isLower value ->
         Just (SymbolNodeType (Symbol.init value))
     | otherwise ->
         String.toFloat value `Maybe.andThen` ConstantNodeType Constant.init
         --case String.toFloat value of
         --  Ok num -> Just (ConstantNodeType (Constant.init num))
         --  _      -> Nothing

-- UPDATE

type Action
    = ExpressionActionType Expression.Action
    | SymbolActionType Symbol.Action
    | ConstantActionType Constant.Action

update : Action -> Model -> Model
update action model =
  case action of
    SymbolActionType symbolAction ->
        case model of
            SymbolNodeType symbol -> SymbolNodeType (Symbol.update symbolAction symbol)
    ConstantActionType constantAction ->
        case model of
            ConstantNodeType constant -> ConstantNodeType (Constant.update constantAction constant)



-- VIEW

view : Signal.Address Action -> Model -> Html
view address model =
    case model of
        SymbolNodeType symbol -> Symbol.view (Signal.forwardTo address SymbolActionType) symbol
        ConstantNodeType constant -> Constant.view (Signal.forwardTo address ConstantActionType) constant


countStyle : Attribute
countStyle =
  style
    [ ("font-size", "20px")
    , ("font-family", "monospace")
    , ("display", "inline-block")
    , ("width", "50px")
    , ("text-align", "center")
    ]
