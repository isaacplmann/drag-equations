module Expression.SymbolNode (Model,Action,init,update,view) where

import Html exposing (..)
import Html.Attributes exposing (style)
import Maybe
--import Html.Events exposing (onClick)


-- MODEL

type alias Model =
  { name: String,
  value: Maybe Float
}

init : String -> Model
init name =
  { name = name,
    value = Nothing
  }


-- UPDATE

type Action = Assign Float

update : Action -> Model -> Model
update action model =
  case action of
    Assign value -> { model | value <- Just value }


-- VIEW

view : Signal.Address Action -> Model -> Html
view address model =
    div [ countStyle ] [ text model.name ]


countStyle : Attribute
countStyle =
  style
    [ ("font-size", "20px")
    , ("font-family", "monospace")
    , ("display", "inline-block")
    , ("width", "50px")
    , ("text-align", "center")
    ]
