module Expression.ListHelper where

import List

verifySameLengthAs : List(a) -> List(b) -> List a
verifySameLengthAs list1 list2 =
  if List.length list1 ==  List.length list2 then
    list1
  else
    []
