module Equation.Term (init,initWithValue,update,view,parse,Action,Model) where

import Html exposing (..)
import Html.Attributes exposing (style)
import Html.Events
import Json.Decode as Json
--import Html.Events exposing (onClick)


-- MODEL

type alias Model =
    { constant: Float,
      exponent: Int
    }

init: Model
init =
  { constant = 1,
    exponent = 0
  }

initWithValue: (Float, Int) -> Model
initWithValue input =
  { constant = fst input,
    exponent = snd input
  }

parse: String -> Maybe Model
parse input =
  Just init


-- UPDATE

type Action
    = NoOp
    | Reset
    | DragStart
    --| Modify Float Int

update : Action -> Model -> Model
update action model =
  case action of
    Reset ->
      init
    DragStart ->
      { model
        | constant <- model.constant+1
      }

--dragEventMailbox : Signal.Mailbox Action
--dragEventMailbox = Signal.Mailbox NoOp

--port dragEvents: Signal String
--port dragEvents =
--  Signal.map (\_ -> "dragEvent") dragEventMailbox.signal
    --Modify constant exponent ->
    --  initWithValue constant exponent


-- VIEW

view : Signal.Address Action -> Int -> Model -> Html
view address id model =
  let symbol =
        case model.exponent of
          0 -> ""
          1 -> "x"
          _ -> "x^"++toString model.exponent
      constant =
        if model.constant==1 && model.exponent>0 then
          ""
        else
          toString model.constant
      options =
        { stopPropagation = True
        , preventDefault = True
        }
  in
    span [ Html.Attributes.draggable "true"
         , Html.Attributes.class "term"
         , Html.Attributes.id ("term"++toString id)
         --, Html.Events.onWithOptions "dragstart" options Json.value (\_ -> Signal.message address DragStart)
         ] [ text (constant++symbol) ]

