module Equation.Expression (init,initWithValue,addAll,update,view, removeTerm,Model,Action) where

import Html exposing (..)
import Html.Attributes exposing (style)
import Equation.Term as Term exposing(init,initWithValue,update,view,parse, Model, Action)
--import Html.Events exposing (onClick)


-- MODEL

type alias Model =
    { terms : List ( ID, Term.Model )
    , nextID : ID
    }

type alias ID = Int

init: Model
init =
  { terms = [],
    nextID = 0
  }

initWithValue: List(Float, Int) -> Model
initWithValue list =
  addAll init list

addAll: Model -> List(Float, Int) -> Model
addAll model list =
  case List.length list of
    1 -> addOne model
      (Maybe.withDefault (0,0) (List.head list))
    _ -> addAll
      (addOne model
       (Maybe.withDefault (0,0) (List.head list))
      ) (Maybe.withDefault [] (List.tail list))

addOne: Model -> (Float, Int) -> Model
addOne model value =
  let newModel = Term.initWithValue value
      newterms = model.terms ++ [ (model.nextID, newModel) ]
  in
    { model |
        terms <- newterms,
        nextID <- model.nextID + 1
    }

mergeModels: List(Model) -> Model
mergeModels list =
  List.foldr mergeModel init list

mergeModel: Model -> Model -> Model
mergeModel first second =
    { first |
      terms <- List.append first.terms second.terms,
      nextID <- max first.nextID second.nextID
    }

verifyNoFailedInit : Model -> List(String) -> Model
verifyNoFailedInit model list2 =
  if List.length model.terms ==  List.length list2 then
    model
  else
    init

-- UPDATE

type Action
    = InsertNew
    | Insert String
    --| InsertAll (List String)
    | RemoveAny
    | Remove ID
    | Modify ID Term.Action

update : Action -> Model -> Model
update action model =
  case action of
    InsertNew ->
      let newNode = ( model.nextID, Term.init )
          newterms = model.terms ++ [ newNode ]
      in
          { model |
              terms <- newterms,
              nextID <- model.nextID + 1
          }

    Insert value ->
      let newModel = Term.parse value
          newterms =
            case newModel of
              Nothing   -> model.terms
              Just node -> model.terms ++ [ (model.nextID, node) ]
      in
          { model |
              terms <- newterms,
              nextID <- model.nextID + 1
          }

    --InsertAll list ->
    --  (List.map Insert list)
    --    |> List.head
    --    |> Maybe.withDefault init

    RemoveAny ->
      { model | terms <- List.drop 1 model.terms }

    Remove id ->
      removeTerm model id

    Modify id nodeAction ->
      let updateNode (nodeID, nodeModel) =
            if nodeID == id
                then (nodeID, Term.update nodeAction nodeModel)
                else (nodeID, nodeModel)
      in
          { model | terms <- List.map updateNode model.terms }

removeTerm: Model -> Int -> Model
removeTerm model id =
  { model | terms <- List.filter (\(tid,term) -> tid/=id) model.terms }

-- VIEW

--view : Signal.Address Action -> Model -> Html
--view address model =
--    div [] [ text (toString model) ]

view : Signal.Address Action -> Model -> Html
view address model =
  let terms = List.map (viewNode address) model.terms
      --remove = button [ onClick address Remove ] [ text "Remove" ]
      --insert = button [ onClick address Insert ] [ text "Add" ]
  in
      span [ Html.Attributes.class "expression" ] (List.intersperse (span [ Html.Attributes.class "operation" ] [text "+"]) terms)


viewNode : Signal.Address Action -> (ID, Term.Model) -> Html
viewNode address (id, model) =
  Term.view (Signal.forwardTo address (Modify id)) id model

