module Equation.Equation (init,initWithValue,update,view, setOperation, removeTerm,Model,Action) where

import Equation.Expression as Expression
import Html exposing (..)
import Html.Attributes exposing (style)
--import Html.Events exposing (onClick)


-- MODEL

type OperationType = Equals
type alias Model =
    { leftSide  : Expression.Model
    , rightSide : Expression.Model
    , operation : OperationType
    }

type alias ID = Int

init: Model
init =
  { leftSide  = Expression.init
  , rightSide = Expression.init
  , operation = Equals
  }

initWithValue: List(Float,Int) -> List(Float,Int) -> Model
initWithValue left right =
  { leftSide  = Expression.initWithValue left
  , rightSide = Expression.initWithValue right
  , operation = Equals
  }

setOperation: String -> Model -> Model
setOperation opString model =
  let operation =
    case opString of
      _   -> Equals
  in
    { model |
      operation <- operation
    }


-- UPDATE

type Action
    = Reset
    | Left Expression.Action
    | Right Expression.Action
    | NoOp

update : Action -> Model -> Model
update action model =
  case action of
    Reset ->
      init

    Left act ->
      { model |
          leftSide <- Expression.update act model.leftSide
      }

    Right act ->
      { model |
          rightSide <- Expression.update act model.rightSide
      }

removeTerm : Model -> String -> Int -> Model
removeTerm model side id =
  case side of
    "right" ->
      { model
      | rightSide <- Expression.removeTerm model.rightSide id
      }
    "left" ->
      { model
      | leftSide <- Expression.removeTerm model.leftSide id
      }

-- VIEW

view : Signal.Address Action -> Model -> Html
view address model =
  div [ Html.Attributes.class "equation" ]
    [ Expression.view (Signal.forwardTo address Left) model.leftSide
    , span [] [ text " = " ]
    , Expression.view (Signal.forwardTo address Right) model.rightSide
    ]
