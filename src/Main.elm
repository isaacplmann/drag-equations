import Html exposing (div, button, text)
import Html.Events exposing (onClick)
import Equation.Equation as Equation exposing (init,update,view,Action,Model)
import Equation.Expression exposing (Action)
import StartApp.Simple as StartApp


model = Equation.initWithValue [(2,1),(3,0)] [(1,1),(4,0)]
-- 2x+3 = x+4


port dragTerm : Signal (Maybe (String,Int))
port dropTerm : Signal (Maybe (Float,Int))

type Action
  = NoOp
  | RemoveTerm

update : Action -> Model -> Model
update action model =
  case action of
    NoOp -> model
    --RemoveTerm -> removeTerm
  --{ model
  --| leftSide <- newmodel.leftSide
  --, rightSide <- newmodel.rightSide
  --, operation <- newmodel.operation
  --}

removeTerm: Signal Model
removeTerm
  = Signal.map
    (\input ->
      case input of
        (Just value) ->
          Equation.removeTerm model (fst value) (snd value)
        Nothing ->
          model
      )
   dragTerm

--model : Signal Model
--model = Signal.foldp update initialModel removeTerm

main = StartApp.start { model = model, view = Equation.view, update = Equation.update }

