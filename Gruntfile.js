
module.exports = function(grunt) {
  htmlminFiles = {"dist/index.html": "src/index.html"}

  grunt.initConfig({
    clean: ["dist"],

    watch: {
      elm: {
        files: ["src/Equation/**/*.elm", "src/StartApp/**/*.elm", "src/*.elm"],
        tasks: ["elm"]
      },
      sass: {
        files: ["src/stylesheets/**/*.scss"],
        tasks: ["sass:dev", "autoprefixer:dev"]
      },
      html: {
        files: ["src/index.html"],
        tasks: ["htmlmin"]
      },
      images: {
        files: ["src/images/*.*"],
        tasks: ["copy:images"]
      },
      fonts: {
        files: ["src/fonts/*.*"],
        tasks: ["copy:fonts"]
      },
      bower: {
        files: ["bower.json"],
        tasks: ["browserifyBower"]
      }
    },

    connect: {
      prod: {
        options: {
          port: 8000,
          base: 'dist',
          keepalive: true
        }
      }
    },

    uglify: {
      prod: {
        options: {
          sourceMap: false
        },
        files: {
          "dist/App.js": "dist/App.js",
          // "dist/vendor.js": "dist/vendor.js",
          "dist/bootstrap-elm.js": "dist/bootstrap-elm.js"
        },
      }
    },

    copy: {
      images: {
        expand: true,
        cwd: "src",
        src: "images/**",
        dest: "dist/"
      },
      fonts: {
        expand: true,
        cwd: "src",
        src: "fonts/**",
        dest: "dist/"
      },
      cache: {
        expand: true,
        cwd: "dist",
        src: ["*.*", "fonts/*.*", "images/*.*"],
        dest: "dist/cache/"
      }
    },

    sass: {
      dev: {
        linenos: true,
        paths: ["src/stylesheets/*.scss"],
        files: {
          "dist/dragequations.css": ["src/stylesheets/*.scss"]
        }
      },
      prod: {
        linenos: false,
        paths: "<%= sass.dev.paths %>",
        files: "<%= sass.dev.files %>"
      }
    },

    autoprefixer: {
      dev: {
        options: {
          map: true,
          src: "dist/dragequations.css",
          dest: "dist/dragequations.css"
        },
      },
      prod: {
        options: {
          map: false,
          src: "<%= autoprefixer.dev.src %>",
          dest: "<%= autoprefixer.dev.dest %>"
        }
      }
    },

    elm: {
      compile: {
        files: {
          'dist/Main.js': 'src/Main.elm'
        }
      }
    },

    browserify: {
      options: {
        transform: ['browserify-mustache', 'coffeeify'],
        watch: true,
        browserifyOptions: {
          debug: true
        }
      },
      dev: {
        extensions: ['.coffee', '.mustache', '.json'],
        src: ["./src/**/*.coffee", "./src/**/*.mustache", "./src/**/*.json"],
        dest: "dist/bootstrap-elm.js"
      },
      prod: {
        extensions: "<%= browserify.dev.extensions %>",
        src: "<%= browserify.dev.src %>",
        dest: "<%= browserify.dev.dest %>"
      }
    },

    browserifyBower: {
      options: {
        file: "dist/vendor.js",
        forceResolve: {
        }
      },
      vendor: {}
    },

    htmlmin: {
      options: {
        removeComments: true,
        collapseWhitespace: true
      },
      dev: {
        files: htmlminFiles
      },
      prod: {
        files: htmlminFiles
      }
    },

    cssmin: {
      dev: {
        files: {
          'dist/dragequations.css': 'dist/dragequations.css'
        }
      },
      prod: {
        files: "<%= cssmin.dev.files %>"
      }
    },

    appcache: {
      options: {
        basePath: 'dist'
      },
      dev: {
        dest: 'dist/dragequations.appcache',
        network: '*'
      },
      prod: {
        dest: 'dist/dragequations.appcache',
        cache: {
          patterns: ['dist/cache/**/*']
        },
        network: '*',
        fallback: [
          '/                               /cache/index.html',
          '/index.html                     /cache/index.html',
          '/dragequations.css                /cache/dragequations.css',
          '/App.js                         /cache/App.js',
          '/vendor.js                      /cache/vendor.js',
          '/bootstrap-elm.js               /cache/bootstrap-elm.js',
          '/fonts/ubuntu.woff              /cache/fonts/ubuntu.woff',
          '/fonts/flaticon.woff            /cache/fonts/flaticon.woff',
          '/images/dlogo.png               /cache/images/dlogo.png',
          '/images/favicon.ico             /cache/images/favicon.ico'
        ]
      }
    },

    browserSync: {
      dev: {
        bsFiles: {
          src : 'dist/**/*'
        },
        options: {
          watchTask: true,
          port: 8000,
          server: {
            baseDir: "dist"
          }
        }
      }
    }
  });

  ["grunt-contrib-watch", "grunt-contrib-htmlmin", "grunt-contrib-cssmin", "grunt-contrib-htmlmin", "grunt-contrib-uglify", "grunt-contrib-clean", "grunt-elm", "grunt-browserify", "grunt-browserify-bower", "grunt-contrib-copy", "grunt-contrib-connect", "grunt-contrib-sass", "grunt-autoprefixer", "grunt-appcache", "grunt-browser-sync"].forEach(function(plugin) {
    grunt.loadNpmTasks(plugin);
  });

  grunt.registerTask("build:prod", ["sass:prod", "autoprefixer:prod", "browserifyBower", "browserify:prod", "elm", "htmlmin:prod", "copy", "uglify:prod", "cssmin:prod", "appcache:prod"]);
  grunt.registerTask("build:dev",  ["sass:dev",  "autoprefixer:dev",  "browserifyBower", "browserify:dev",  "elm", "htmlmin:dev",  "copy",                               "appcache:dev"]);

  grunt.registerTask("build",  ["build:dev"]);
  grunt.registerTask("prod",   ["build:prod", "connect:prod"]);
  grunt.registerTask("deploy", ["clean", "build:prod"]);

  grunt.registerTask("default", ["clean", "build", "browserSync:dev", "watch"]);
};
